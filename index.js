
const imageList = ['/1.jpg', '/2.jpg', '/3.jpg', '/4.png',];


const container = document.querySelector('.images-wrapper');
const stopButton = document.querySelector('.stop');
const nextButton = document.querySelector('.next');

let currentImageIndex = 0;
let timerId;

let renderImage = () => {
  timerId = setInterval(() => {
    let currentImage = imageList[currentImageIndex];
    // console.log(currentImage)
    container.innerHTML = `<img src="./img${currentImage}" class="image-to-show">`;
    currentImageIndex++;
    // console.log(currentImageIndex)
    if (currentImageIndex > imageList.length - 1) {
      currentImageIndex = 0;
    }
  }, 3000);
};

renderImage();

stopButton.addEventListener('click', () => {
  clearInterval(timerId);
});

nextButton.addEventListener('click', () => {
    renderImage();
});